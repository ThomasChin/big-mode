from django.contrib.auth.models import AbstractUser
from django.db import models

from .managers import UserManager


class User(AbstractUser):
    objects = UserManager()
    first_name = models.CharField(max_length=128, null=True, blank=True)
    last_name = models.CharField(max_length=128, null=True, blank=True)
    email = models.EmailField("Email Address", unique=True)

    def __str__(self):
        return "{}".format(self.email) # EmailField is a subclass of CharField

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]
