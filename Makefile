compose_file := docker-compose.yml
compose := docker-compose -f $(compose_file)

.PHONY: black
black: 
	$(compose) exec web black .

.PHONY: clean
clean:
	rm -rf .docker-data/

.PHONY: compose-build
compose-build:
	$(compose) build

.PHONY: compose-down
compose-down:
	$(compose) down

.PHONY: compose-shell
compose-shell:
	$(compose) exec web sh

.PHONY: compose-up
compose-up:
	$(compose) up -d

.PHONY: create_superuser
create_superuser:
	$(compose) exec web python manage.py createsuperuser
